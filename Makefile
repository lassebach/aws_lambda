IMAGE_NAME ?= grib_to_parquet
TAG ?= latest

IMAGE_NAME_AND_TAG = $(IMAGE_NAME):$(TAG)

REGISTRY ?= 874986987755.dkr.ecr.eu-west-1.amazonaws.com


docker-build: Dockerfile
	docker build . -t $(IMAGE_NAME_AND_TAG)

docker-run-lambda: docker-build
	docker run -it -p 9000:8080 --rm $(IMAGE_NAME_AND_TAG)

docker-run-%: docker-build
	docker run -it --rm --entrypoint '' $(IMAGE_NAME_AND_TAG) $*

clean:
	find . -type f -name '*.py[co]' -delete -o -type d -name '__pycache__' -delete



_docker-login:
	aws ecr get-login-password | \
	docker login --username AWS --password-stdin $(REGISTRY)


docker-push: REGISTRY_IMAGE_NAME_AND_TAG = $(REGISTRY)/$(IMAGE_NAME_AND_TAG)
docker-push: docker-build _docker-login
	docker tag $(IMAGE_NAME_AND_TAG) $(REGISTRY_IMAGE_NAME_AND_TAG)
	docker push $(REGISTRY_IMAGE_NAME_AND_TAG)
