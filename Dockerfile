FROM python:3.8-slim


RUN apt-get update \
  && apt-get install -y \
  g++ \
  make \
  cmake \
  unzip \
  curl \
  libcurl4-openssl-dev \
  libeccodes0 \
  && rm -rf /var/lib/apt/lists/*

RUN python -m pip install -U pip \
    && python -m pip install poetry \
    && poetry config virtualenvs.create false

RUN curl -Lo /bin/aws-lambda-rie https://github.com/aws/aws-lambda-runtime-interface-emulator/releases/latest/download/aws-lambda-rie \
    && chmod +x /bin/aws-lambda-rie

ADD entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

RUN mkdir /lambda/
ADD pyproject.toml poetry.lock /lambda/
WORKDIR /lambda
RUN poetry install --no-root

ADD lambda-src/ /lambda/lambda-src
RUN poetry install

ADD *.grib2 /
ENTRYPOINT ["/entrypoint.sh"]
CMD ["app.grib_to_parquet.handler"]
