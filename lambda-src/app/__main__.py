from .grib_to_parquet import open_grib

def main():
    filename = "/sof-d.20200101.t00z.0p125.basic.global.f000.grib2"
    datasets = open_grib(filename)
    print(datasets)


if __name__ == "__main__":
    main()
