import xarray


def open_grib(filename, backend_kwargs_indexpath=None):
    # Todo: Can we open directly from s3?
    return xarray.open_dataset(
        filename,
        engine="cfgrib",
        backend_kwargs={"indexpath": backend_kwargs_indexpath})


def handler(event, context):
    filename = "/sof-d.20200101.t00z.0p125.basic.global.f000.grib2" # file in docker container
    datasets = open_grib(filename)
    print(datasets)
    return f"Event: '{event}'\nContext: '{context}'"
